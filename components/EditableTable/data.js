export const data = [
  {"transportWay":1,"transportTool":4,"acceptInsurance":true,"insureFeeRate":0.01,"minInsureFee":1000,"settlementFeeRate":0.001,"insureType":"险别1"},
  {"transportWay":2,"transportTool":4,"acceptInsurance":false,"insureFeeRate":"","minInsureFee":"","settlementFeeRate":"","insureType":""},
  {"transportWay":4,"transportTool":4,"acceptInsurance":false,"insureFeeRate":"","minInsureFee":"","settlementFeeRate":"","insureType":""},
  {"transportWay":5,"transportTool":4,"acceptInsurance":false,"insureFeeRate":"","minInsureFee":"","settlementFeeRate":"","insureType":""},
  {"transportWay":6,"transportTool":4,"acceptInsurance":false,"insureFeeRate":"","minInsureFee":"","settlementFeeRate":"","insureType":""}
];

export const template = [
  {
    "transportType":"公路",
    "transportTypeId": 1,
    "transportToolId": 4,
    "transportTool": "全封闭集装箱",
    "code": 1
  },
  {
    "transportType":"铁路",
    "transportTypeId": 2,
    "transportToolId": 4,
    "transportTool": "全封闭集装箱",
    "code": 2
  },
  {
    "transportType":"水路",
    "transportTypeId": 3,
    "transportToolId": 4,
    "transportTool": "全封闭集装箱",
    "code": 3
  },
  {
    "transportType":"海运",
    "transportTypeId": 4,
    "transportToolId": 4,
    "transportTool": "全封闭集装箱",
    "code": 4
  },
  {
    "transportType":"航空",
    "transportTypeId": 5,
    "transportToolId": 4,
    "transportTool": "全封闭集装箱",
    "code": 5
  },
  {
    "transportType":"快递",
    "transportTypeId": 6,
    "transportToolId": 4,
    "transportTool": "全封闭集装箱",
    "code": 6
  },
];